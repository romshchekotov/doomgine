#!/bin/zsh
find shaders -regextype sed -regex "shaders/.*\.\(vert\|frag\)" -exec zsh -c "echo '\x1b[38;2;128;160;188mCompiling {}...\x1b[0m' && glslc {} -o build/{}.spv" \;
touch shader-comp-success
rm shader-comp-success

# C++ Graphics Project w/ Vulkan

This is a simple 3D Graphics Projects using Vulkan, GLFW & GLM.
This project is made for learning the related Tech and is based on 
[a YouTube tutorial](https://www.youtube.com/watch?v=Y9U9IE0gVHA&list=PL8327DO66nu9qYVKLDmdLW_84-yE4auCR).

Another major part of this project is the testing using G(oogle)Test - 
the C++ Unit Test suite.

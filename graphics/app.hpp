#ifndef APP_HPP
#define APP_HPP

#include <memory>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "doom_device.hpp"
#include "doom_model.hpp"
#include "doom_pipeline.hpp"
#include "doom_swap_chain.hpp"
#include "doom_window.hpp"

namespace doom {
class App {
  public:
    static constexpr int WIDTH = 800;
    static constexpr int HEIGHT = 600;

    App();
    ~App();

    App(const App &) = delete;
    App &operator=(const App &) = delete;

    void run();

  private:
    void loadModels();
    void createPipelineLayout();
    void createPipeline();
    void createCommandBuffers();
    void drawFrame();

    DoomWindow window{WIDTH, HEIGHT, "Doom Window"};
    DoomDevice device{window};
    DoomSwapChain doomSwapChain{device, window.getExtent()};
    std::unique_ptr<DoomPipeline> pipeline;
    VkPipelineLayout pipelineLayout;
    std::vector<VkCommandBuffer> commandBuffers;
    std::unique_ptr<DoomModel> doomModel;
};
}

#endif

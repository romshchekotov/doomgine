#ifndef DOOM_MODELS_HPP
#define DOOM_MODELS_HPP

#include <vector>
#include "doom_model.hpp"

namespace doom {
  std::vector<DoomModel::Vertex> sierpinski(int depth);
}

#endif

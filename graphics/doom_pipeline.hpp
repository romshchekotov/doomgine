#ifndef DOOM_PIPELINE_HPP
#define DOOM_PIPELINE_HPP

#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "doom_device.hpp"

namespace doom {
  struct PipelineConfigInfo {
    VkViewport viewport;
    VkRect2D scissor;
    VkPipelineInputAssemblyStateCreateInfo inputAssemlyInfo;
    VkPipelineRasterizationStateCreateInfo rasterizationInfo;
    VkPipelineMultisampleStateCreateInfo multisampleInfo;
    VkPipelineColorBlendAttachmentState colorBlendAttachment;
    VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
    VkPipelineLayout pipelineLayout = nullptr;
    VkRenderPass renderPass = nullptr;
    uint32_t subpass = 0;
  };

  class DoomPipeline {
    public:
      DoomPipeline(
          DoomDevice& device, 
          const std::string& vertPath, 
          const std::string& fragPath,
          const PipelineConfigInfo& config
      );
      ~DoomPipeline();

      DoomPipeline(const DoomPipeline&) = delete;
      void operator=(const DoomPipeline&) = delete;

      void bind(VkCommandBuffer buffer);

      static PipelineConfigInfo defaultPipelineConfigInfo(uint32_t width, uint32_t height);

    private:
      static std::vector<char> readFile(const std::string& path);
      
      void createGraphicsPipeline(
          const std::string& vertPath, 
          const std::string& fragPath,
          const PipelineConfigInfo& config
      );

      void createShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule);

      DoomDevice& device;
      VkPipeline graphicsPipeline;
      VkShaderModule vertShaderModule;
      VkShaderModule fragShaderModule;
  };
}

#endif

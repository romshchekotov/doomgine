#ifndef DOOM_WINDOW_HPP
#define DOOM_WINDOW_HPP

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <string>

namespace doom {
class DoomWindow {
  public:
    DoomWindow(int width, int height, std::string name);
    ~DoomWindow();

    DoomWindow(const DoomWindow &) = delete;
    DoomWindow &operator=(const DoomWindow &) = delete;

    bool shouldClose() { return glfwWindowShouldClose(window); };
    VkExtent2D getExtent() { return { static_cast<uint32_t>(width), static_cast<uint32_t>(height) }; };
    void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);

  private:
    void initWindow();

    const int width;
    const int height;

    std::string windowName;
    GLFWwindow* window;
};
}

#endif

#include "doom_models.hpp"

namespace doom {
  /**
   *  Generates the coordinates for a Sierpinski Triangle
   *  @param depth - 0-based 'recursion'-depth
   */
  std::vector<DoomModel::Vertex> sierpinski(int depth) {
    int batch_size = std::pow(3, depth); 
    int length = 3 * batch_size; 
    int batches = 3; 

    std::vector<DoomModel::Vertex> result{};
    result.resize(length);

    // initial values
    result.at(0) = {{0, -0.5}};
    result.at(length/2) = {{0.5, 0.5}};
    result.at(length - 1) = {{-0.5, 0.5}};

    while(batch_size > 2) {
      for(int batch = 0; batch < batches; batch++) {
        int parent_batch = batch / 3;
        int pos_in_parent = batch % 3;
        int parent_size = batch_size * 3;
        DoomModel::Vertex a = result.at(parent_size * parent_batch);
        DoomModel::Vertex b = result.at(parent_size * parent_batch + parent_size/2);
        DoomModel::Vertex c = result.at(parent_size * (parent_batch + 1) - 1);
        glm::vec2 dp = (a.position + b.position);
        DoomModel::Vertex d = {{ dp.x / 2.0f, dp.y / 2.0f }};
        glm::vec2 ep = (a.position + c.position);
        DoomModel::Vertex e = {{ ep.x / 2.0f, ep.y / 2.0f }};
        DoomModel::Vertex f = {{ a.position.x, c.position.y }};
        
        int offset = batch * batch_size;
        int half = batch_size >> 1;
        if(pos_in_parent == 0) {
          result.at(offset + half) = d;
          result.at(offset + 2 * half) = e;
        } else if(pos_in_parent == 1) {
          result.at(offset) = d;
          result.at(offset + 2 * half) = f;
        } else if(pos_in_parent == 2) {
          result.at(offset) = e;
          result.at(offset + half) = f;
        }
      }
      batch_size /= 3;
      batches *= 3;
    }

    return result;
  }
}

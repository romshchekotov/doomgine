#include "app.hpp"
#include "doom_model.hpp"
#include "doom_models.hpp"
#include "doom_pipeline.hpp"

#include <iostream>
#include <GLFW/glfw3.h>
#include <cmath>
#include <cstdint>
#include <glm/exponential.hpp>
#include <glm/fwd.hpp>
#include <memory>
#include <vector>
#include <vulkan/vulkan_core.h>
#include <stdexcept>

namespace doom {
  App::App() {
    loadModels();
    createPipelineLayout();
    createPipeline();
    createCommandBuffers();
  }

  App::~App() {
    vkDestroyPipelineLayout(device.device(), pipelineLayout, nullptr);
  }

  void App::run() {
    while(!window.shouldClose()) {
      glfwPollEvents();
      drawFrame();
    }

    vkDeviceWaitIdle(device.device());
  }

  void App::createPipelineLayout() {
    VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 0;
    pipelineLayoutInfo.pSetLayouts = nullptr;
    pipelineLayoutInfo.pushConstantRangeCount = 0;
    pipelineLayoutInfo.pPushConstantRanges = nullptr;
   
    if(vkCreatePipelineLayout(device.device(), &pipelineLayoutInfo,
          nullptr, &pipelineLayout) != VK_SUCCESS) {
      throw std::runtime_error("failed to create pipeline layout!");
    }
  }

  void App::createPipeline() {
    auto pipelineConfig = DoomPipeline::defaultPipelineConfigInfo(doomSwapChain.width(), doomSwapChain.height());
    pipelineConfig.renderPass = doomSwapChain.getRenderPass();
    pipelineConfig.pipelineLayout = pipelineLayout;
    pipeline = std::make_unique<DoomPipeline>(
        device, "shaders/simple_shader.vert.spv",
        "shaders/simple_shader.frag.spv", pipelineConfig);
  }

  void App::createCommandBuffers() {
    commandBuffers.resize(doomSwapChain.imageCount());
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = device.getCommandPool();
    allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

    if(vkAllocateCommandBuffers(device.device(), &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
      throw std::runtime_error("failed to allocate command buffers");
    }

    for(int i = 0; i < commandBuffers.size(); i++) {
      VkCommandBufferBeginInfo beginInfo{};
      beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

      if(vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS) {
        throw std::runtime_error("failed to begin recording command buffer!");
      }

      VkRenderPassBeginInfo renderPassInfo{};
      renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
      renderPassInfo.renderPass = doomSwapChain.getRenderPass();
      renderPassInfo.framebuffer = doomSwapChain.getFrameBuffer(i);

      renderPassInfo.renderArea.offset = {0, 0};
      renderPassInfo.renderArea.extent = doomSwapChain.getSwapChainExtent();

      std::array<VkClearValue, 2> clearValues{};
      clearValues[0].color = {0.1f, 0.1f, 0.1f, 1.0f};
      clearValues[1].depthStencil = {1.0f, 0};
      renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
      renderPassInfo.pClearValues = clearValues.data();

      vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

      pipeline->bind(commandBuffers[i]);
      doomModel->bind(commandBuffers[i]);
      doomModel->draw(commandBuffers[i]);

      vkCmdEndRenderPass(commandBuffers[i]);
      if(vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
        throw std::runtime_error("failed to record command buffer!");
      }
    }
  }

  void App::drawFrame() {
    uint32_t imageIndex;
    auto result = doomSwapChain.acquireNextImage(&imageIndex);
    if(!(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR)) {
      throw std::runtime_error("failed to acquire swap image!");
    }

    result = doomSwapChain.submitCommandBuffers(&commandBuffers[imageIndex], &imageIndex);
    if(result != VK_SUCCESS) {
      throw std::runtime_error("failed to present swap chain image!");
    }
  }

  float mid(float less, float more) {
    return (more - less)/2 + less;
  }

  void print_vertex_array(std::vector<DoomModel::Vertex> &array) {
    for(auto i = (&array)->begin(); i != (&array)->end(); i++) {
      std::cout << '(' << i->position.x << '|' << i->position.y << ')' << std::endl;
    }
  }


  void App::loadModels() {
    std::vector<DoomModel::Vertex> vertices = sierpinski(3);
    doomModel = std::make_unique<DoomModel>(device, vertices);
  }
}

#include "doom_window.hpp"

#include <GLFW/glfw3.h>
#include <stdexcept>

namespace doom {
  DoomWindow::DoomWindow(int width, int height, std::string name) : 
    width{width}, height{height}, windowName{name} {
    initWindow();
  }

  DoomWindow::~DoomWindow() {
    glfwDestroyWindow(window);
    glfwTerminate();
  }

  void DoomWindow::initWindow() {
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    window = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
  }


  void DoomWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface) {
    if(glfwCreateWindowSurface(instance, window, nullptr, surface)) {
      throw std::runtime_error("failed to create window surface");
    }
  }
}

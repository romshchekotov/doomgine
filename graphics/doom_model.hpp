#ifndef DOOMGINE_DOOM_MODEL_HPP
#define DOOMGINE_DOOM_MODEL_HPP

#include "doom_device.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <vector>

namespace doom {
  class DoomModel {
  public:
    struct Vertex {
      glm::vec2 position;

      static std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
      static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
    };

    DoomModel(DoomDevice &device, const std::vector<Vertex> &vertices);
    ~DoomModel();

    DoomModel(const DoomModel &) = delete;
    DoomModel &operator=(const DoomModel &) = delete;

    void bind(VkCommandBuffer commandBuffer);
    void draw(VkCommandBuffer commandBuffer);

  private:
    void createVertexBuffers(const std::vector<Vertex> &vertices);

    DoomDevice& device;
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    uint32_t vertexCount;
  };
}

#endif //DOOMGINE_DOOM_MODEL_HPP
